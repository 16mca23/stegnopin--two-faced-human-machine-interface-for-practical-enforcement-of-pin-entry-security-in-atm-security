﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Post_Comp : System.Web.UI.Page
{
    dboperation db = new dboperation();
    SqlCommand cmd = new SqlCommand();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        cmd.CommandText = "select max(cid) from complaint";
        id = db.max_id(cmd);
        cmd.CommandText = "insert into complaint values('"+id+"','"+Session["id"]+"','"+TextBox1.Text+"','pending','"+System.DateTime.Now.ToShortDateString()+"')";
        db.execute(cmd);
        Response.Write("<script>alert('Complaint posted')</script>");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View2);
        cmd.CommandText = "select com,reply from complaint where uid='" + Session["id"] + "'";
        DataGrid1.DataSource = db.getdata(cmd);
        DataGrid1.DataBind();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("e_hom.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
    }
}