﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;


public partial class Register_Emp : System.Web.UI.Page
{
    dboperation db = new dboperation();
    SqlCommand cmd = new SqlCommand();
    static int id;
    static string uid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);
            cmd.CommandText = "select * from employee";
            DataGrid1.DataSource = db.getdata(cmd);
            DataGrid1.DataBind();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        cmd.CommandText = "insert into employee values('" + id + "','" + TextBox1.Text + "','" + TextBox2.Text + "','" + RadioButtonList1.Text + "','" + TextBox4.Text + "','" + TextBox5.Text + "','" + TextBox6.Text + "','"+TextBox11.Text+"')";
        db.execute(cmd);
        cmd.CommandText = "insert into login values('" + id + "','" + TextBox6.Text + "','" + TextBox11.Text + "','employee')";
        db.execute(cmd);
        Response.Write("<script>alert('Registered')</script>");
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from employee";
        DataGrid1.DataSource = db.getdata(cmd);
        DataGrid1.DataBind();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View2);
        cmd.CommandText = "select max(emp_id) from employee";
        id = db.max_id(cmd);
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.CommandName == "update")
        {
            MultiView1.SetActiveView(View3);
            uid = e.Item.Cells[0].Text;
            TextBox7.Text = e.Item.Cells[1].Text;
            TextBox8.Text = e.Item.Cells[4].Text;
            TextBox9.Text = e.Item.Cells[5].Text;
            TextBox10.Text = e.Item.Cells[6].Text;
        }
        else
        {
            cmd.CommandText = "delete from employee where emp_id='"+uid+"'";
            db.execute(cmd);
            Response.Write("<script>alert('Deleted')</script>");
            MultiView1.SetActiveView(View1);
            cmd.CommandText = "select * from employee";
            DataGrid1.DataSource = db.getdata(cmd);
            DataGrid1.DataBind();
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        cmd.CommandText="update employee set post='"+TextBox8.Text+"',adrs='"+TextBox9.Text+"',mail='"+TextBox10.Text+"' where emp_id='"+uid+"'";
        db.execute(cmd);
        Response.Write("<script>alert('Updated')</script>");
        MultiView1.SetActiveView(View1);
        cmd.CommandText = "select * from employee";
        DataGrid1.DataSource = db.getdata(cmd);
        DataGrid1.DataBind();
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("a_hom.aspx");
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        MultiView1.SetActiveView(View1);
    }
}