﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.master" AutoEventWireup="true" CodeFile="Register_Emp.aspx.cs" Inherits="Register_Emp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 343px;
        }
        .style3
        {
            width: 333px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table class="style1">
        <tr>
            <td>
                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <asp:DataGrid ID="DataGrid1" runat="server" AutoGenerateColumns="False" 
                            BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" 
                            CellPadding="3" CellSpacing="1" Font-Bold="False" Font-Italic="False" 
                            Font-Overline="False" Font-Strikeout="False" Font-Underline="False" 
                            GridLines="None" Height="169px" HorizontalAlign="Center" 
                            onitemcommand="DataGrid1_ItemCommand" Width="663px">
                            <Columns>
                                <asp:BoundColumn DataField="emp_id" HeaderText="Emp_id"></asp:BoundColumn>
                                <asp:BoundColumn DataField="name" HeaderText="Name"></asp:BoundColumn>
                                <asp:BoundColumn DataField="dob" HeaderText="DOB"></asp:BoundColumn>
                                <asp:BoundColumn DataField="gender" HeaderText="Gender"></asp:BoundColumn>
                                <asp:BoundColumn DataField="post" HeaderText="Post"></asp:BoundColumn>
                                <asp:BoundColumn DataField="adrs" HeaderText="Address"></asp:BoundColumn>
                                <asp:BoundColumn DataField="mail" HeaderText="Mail"></asp:BoundColumn>
                                <asp:ButtonColumn CommandName="update" Text="Update"></asp:ButtonColumn>
                                <asp:ButtonColumn CommandName="delete" Text="Delete"></asp:ButtonColumn>
                            </Columns>
                            <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                            <ItemStyle BackColor="#DEDFDE" ForeColor="Black" />
                            <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                            <SelectedItemStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                        </asp:DataGrid>
                        <br />
                        <asp:Button ID="Button3" runat="server" Height="32px" onclick="Button3_Click" 
                            Text="Add new" Width="149px" />
                        &nbsp;
                        <asp:Button ID="Button4" runat="server" Height="25px" onclick="Button4_Click" 
                            Text="Back" Width="101px" />
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <table class="style1">
                            <tr>
                                <td class="style2">
                                    Name</td>
                                <td>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    DOB</td>
                                <td>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    Gender</td>
                                <td>
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" 
                                        RepeatDirection="Horizontal">
                                        <asp:ListItem>Male</asp:ListItem>
                                        <asp:ListItem>Female</asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    Post</td>
                                <td>
                                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    Address</td>
                                <td>
                                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    Email</td>
                                <td>
                                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    Password</td>
                                <td>
                                    <asp:TextBox ID="TextBox11" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                                <td>
                                    <asp:Button ID="Button1" runat="server" Height="29px" onclick="Button1_Click" 
                                        Text="Register" Width="125px" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="Button5" runat="server" Height="27px" onclick="Button5_Click" 
                                        Text="Back" Width="103px" />
                                </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </asp:View>
                       <asp:View ID="View3" runat="server">
                           <table class="style1">
                               <tr>
                                   <td class="style3">
                                       Name</td>
                                   <td>
                                       <asp:TextBox ID="TextBox7" runat="server" ReadOnly="True"></asp:TextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style3">
                                       Post</td>
                                   <td>
                                       <asp:TextBox ID="TextBox8" runat="server"></asp:TextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style3">
                                       Address</td>
                                   <td>
                                       <asp:TextBox ID="TextBox9" runat="server"></asp:TextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style3">
                                       Email</td>
                                   <td>
                                       <asp:TextBox ID="TextBox10" runat="server"></asp:TextBox>
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style3">
                                       &nbsp;</td>
                                   <td>
                                       <asp:Button ID="Button2" runat="server" Height="30px" onclick="Button2_Click" 
                                           Text="Update" Width="134px" />
                                       &nbsp;
                                       <asp:Button ID="Button6" runat="server" Height="28px" onclick="Button6_Click" 
                                           Text="Back" Width="109px" />
                                   </td>
                               </tr>
                               <tr>
                                   <td class="style3">
                                       &nbsp;</td>
                                   <td>
                                       &nbsp;</td>
                               </tr>
                           </table>
                       </asp:View>
                </asp:MultiView>
            </td>
        </tr>
    </table>
</asp:Content>

