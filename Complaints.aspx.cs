﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Complaints : System.Web.UI.Page
{
    dboperation db = new dboperation();
    SqlCommand cmd = new SqlCommand();
    string id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            MultiView1.SetActiveView(View1);
            cmd.CommandText = "select * from complaint where reply='pending'";
            DataGrid1.DataSource = db.getdata(cmd);
            DataGrid1.DataBind();
        }
    }
    protected void DataGrid1_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        MultiView1.SetActiveView(View2);
        id=e.Item.Cells[0].Text;
        TextBox1.Text=e.Item.Cells[3].Text;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        cmd.CommandText = "update complaint set reply='" + TextBox2.Text + "' where cid='" + id + "'";
        db.execute(cmd);
        Response.Write("<script>alert('Replied')</script>");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("a_hom.aspx");
    }
}